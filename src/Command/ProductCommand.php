<?php


namespace App\Command;


use App\Entity\SsProducts;
use App\Repository\SsProductRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProductCommand extends CommandAbstract
{

    protected static $defaultName = 'app:product-img';

    /**
     * @var SsProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManager
     */
    private $entityManager;

    public function __construct(SsProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct(null);
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /**
         * @var SsProducts $product
         */
        foreach ($this->productRepository->findAll() as $product) {
            $img = $product->getBigPicture();
            $product->setPicture($img)->setThumbnail($img);
        }

        $this->entityManager->flush();
        $this->successMsg($output);
        return 1;
    }
}
