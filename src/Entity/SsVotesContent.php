<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsVotesContent
 *
 * @ORM\Table(name="SS_votes_content")
 * @ORM\Entity
 */
class SsVotesContent
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="votesID", type="integer", nullable=true)
     */
    private $votesid;

    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="question", type="string", length=50, nullable=true)
     */
    private $question;

    /**
     * @var int|null
     *
     * @ORM\Column(name="result", type="integer", nullable=true)
     */
    private $result;

    public function getVotesid(): ?int
    {
        return $this->votesid;
    }

    public function setVotesid(?int $votesid): self
    {
        $this->votesid = $votesid;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(?string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getResult(): ?int
    {
        return $this->result;
    }

    public function setResult(?int $result): self
    {
        $this->result = $result;

        return $this;
    }


}
