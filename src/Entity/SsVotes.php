<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsVotes
 *
 * @ORM\Table(name="SS_votes")
 * @ORM\Entity
 */
class SsVotes
{
    /**
     * @var int
     *
     * @ORM\Column(name="votesID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $votesid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=true)
     */
    private $title;

    /**
     * @var int|null
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;

    public function getVotesid(): ?int
    {
        return $this->votesid;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getEnable(): ?int
    {
        return $this->enable;
    }

    public function setEnable(?int $enable): self
    {
        $this->enable = $enable;

        return $this;
    }


}
