<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsMenuElement
 *
 * @ORM\Table(name="SS_menu_element", indexes={@ORM\Index(name="id_menu", columns={"id_menu"})})
 * @ORM\Entity
 */
class SsMenuElement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="parent", type="integer", nullable=false)
     */
    private $parent;

    /**
     * @var int
     *
     * @ORM\Column(name="id_elem", type="integer", nullable=false)
     */
    private $idElem;

    /**
     * @var string
     *
     * @ORM\Column(name="hurl", type="string", length=250, nullable=false)
     */
    private $hurl;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10, nullable=false)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer", nullable=false)
     */
    private $sort;

    /**
     * @var \SsMenu
     *
     * @ORM\ManyToOne(targetEntity="SsMenu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_menu", referencedColumnName="id")
     * })
     */
    private $idMenu;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParent(): ?int
    {
        return $this->parent;
    }

    public function setParent(int $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getIdElem(): ?int
    {
        return $this->idElem;
    }

    public function setIdElem(int $idElem): self
    {
        $this->idElem = $idElem;

        return $this;
    }

    public function getHurl(): ?string
    {
        return $this->hurl;
    }

    public function setHurl(string $hurl): self
    {
        $this->hurl = $hurl;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSort(): ?int
    {
        return $this->sort;
    }

    public function setSort(int $sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    public function getIdMenu(): ?SsMenu
    {
        return $this->idMenu;
    }

    public function setIdMenu(?SsMenu $idMenu): self
    {
        $this->idMenu = $idMenu;

        return $this;
    }


}
