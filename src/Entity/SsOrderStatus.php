<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsOrderStatus
 *
 * @ORM\Table(name="SS_order_status")
 * @ORM\Entity
 */
class SsOrderStatus
{
    /**
     * @var int
     *
     * @ORM\Column(name="statusID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $statusid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status_name", type="string", length=90, nullable=true)
     */
    private $statusName;

    /**
     * @var string
     *
     * @ORM\Column(name="group_name", type="text", length=65535, nullable=false)
     */
    private $groupName;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    public function getStatusid(): ?int
    {
        return $this->statusid;
    }

    public function getStatusName(): ?string
    {
        return $this->statusName;
    }

    public function setStatusName(?string $statusName): self
    {
        $this->statusName = $statusName;

        return $this;
    }

    public function getGroupName(): ?string
    {
        return $this->groupName;
    }

    public function setGroupName(string $groupName): self
    {
        $this->groupName = $groupName;

        return $this;
    }

    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }


}
