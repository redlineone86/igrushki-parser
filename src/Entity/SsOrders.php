<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsOrders
 *
 * @ORM\Table(name="SS_orders", indexes={@ORM\Index(name="status", columns={"status"})})
 * @ORM\Entity
 */
class SsOrders
{
    /**
     * @var int
     *
     * @ORM\Column(name="orderID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orderid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="order_time", type="datetime", nullable=true)
     */
    private $orderTime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_firstname", type="string", length=60, nullable=true)
     */
    private $custFirstname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_lastname", type="string", length=60, nullable=true)
     */
    private $custLastname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_email", type="string", length=255, nullable=true)
     */
    private $custEmail;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_country", type="string", length=30, nullable=true)
     */
    private $custCountry;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_zip", type="string", length=30, nullable=true)
     */
    private $custZip;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_state", type="string", length=30, nullable=true)
     */
    private $custState;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_city", type="string", length=70, nullable=true)
     */
    private $custCity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_address", type="text", length=0, nullable=true)
     */
    private $custAddress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cust_phone", type="string", length=30, nullable=true)
     */
    private $custPhone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    /**
     * @var int|null
     *
     * @ORM\Column(name="manager", type="integer", nullable=true)
     */
    private $manager;

    /**
     * @var int|null
     *
     * @ORM\Column(name="custID", type="integer", nullable=true)
     */
    private $custid;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="promocode", type="boolean", nullable=true)
     */
    private $promocode = '0';

    /**
     * @var \SsOrderStatus
     *
     * @ORM\ManyToOne(targetEntity="SsOrderStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status", referencedColumnName="statusID")
     * })
     */
    private $status;

    public function getOrderid(): ?int
    {
        return $this->orderid;
    }

    public function getOrderTime(): ?\DateTimeInterface
    {
        return $this->orderTime;
    }

    public function setOrderTime(?\DateTimeInterface $orderTime): self
    {
        $this->orderTime = $orderTime;

        return $this;
    }

    public function getCustFirstname(): ?string
    {
        return $this->custFirstname;
    }

    public function setCustFirstname(?string $custFirstname): self
    {
        $this->custFirstname = $custFirstname;

        return $this;
    }

    public function getCustLastname(): ?string
    {
        return $this->custLastname;
    }

    public function setCustLastname(?string $custLastname): self
    {
        $this->custLastname = $custLastname;

        return $this;
    }

    public function getCustEmail(): ?string
    {
        return $this->custEmail;
    }

    public function setCustEmail(?string $custEmail): self
    {
        $this->custEmail = $custEmail;

        return $this;
    }

    public function getCustCountry(): ?string
    {
        return $this->custCountry;
    }

    public function setCustCountry(?string $custCountry): self
    {
        $this->custCountry = $custCountry;

        return $this;
    }

    public function getCustZip(): ?string
    {
        return $this->custZip;
    }

    public function setCustZip(?string $custZip): self
    {
        $this->custZip = $custZip;

        return $this;
    }

    public function getCustState(): ?string
    {
        return $this->custState;
    }

    public function setCustState(?string $custState): self
    {
        $this->custState = $custState;

        return $this;
    }

    public function getCustCity(): ?string
    {
        return $this->custCity;
    }

    public function setCustCity(?string $custCity): self
    {
        $this->custCity = $custCity;

        return $this;
    }

    public function getCustAddress(): ?string
    {
        return $this->custAddress;
    }

    public function setCustAddress(?string $custAddress): self
    {
        $this->custAddress = $custAddress;

        return $this;
    }

    public function getCustPhone(): ?string
    {
        return $this->custPhone;
    }

    public function setCustPhone(?string $custPhone): self
    {
        $this->custPhone = $custPhone;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getManager(): ?int
    {
        return $this->manager;
    }

    public function setManager(?int $manager): self
    {
        $this->manager = $manager;

        return $this;
    }

    public function getCustid(): ?int
    {
        return $this->custid;
    }

    public function setCustid(?int $custid): self
    {
        $this->custid = $custid;

        return $this;
    }

    public function getPromocode(): ?bool
    {
        return $this->promocode;
    }

    public function setPromocode(?bool $promocode): self
    {
        $this->promocode = $promocode;

        return $this;
    }

    public function getStatus(): ?SsOrderStatus
    {
        return $this->status;
    }

    public function setStatus(?SsOrderStatus $status): self
    {
        $this->status = $status;

        return $this;
    }


}
