<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SsPayment
 *
 * @ORM\Table(name="SS_payment")
 * @ORM\Entity
 */
class SsPayment
{
    /**
     * @var int
     *
     * @ORM\Column(name="payID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $payid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    private $enabled;

    public function getPayid(): ?int
    {
        return $this->payid;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }


}
