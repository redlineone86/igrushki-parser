<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Proxy\Proxy;

/**
 * SsProducts
 *
 * @ORM\Table(name="SS_products")
 * @ORM\Entity
 */
class SsProducts
{

    /**
     * @ORM\ManyToOne(targetEntity="SsCategories")
     * @ORM\JoinColumn(name="categoryID", referencedColumnName="categoryID")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity="SsBrand")
     * @ORM\JoinColumn(name="brandID", referencedColumnName="brandID")
     */
    private $brand;

    /**
     * @ORM\OneToMany(targetEntity="SsProductOptionsValues", mappedBy="product")
     */
    private $variants;

    /**
     * @var int
     *
     * @ORM\Column(name="productID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="categoryID", type="integer", nullable=true)
     */
    private $categoryid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=0, nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="customers_rating", type="float", precision=10, scale=0, nullable=false)
     */
    private $customersRating = 5;

    /**
     * @var string|null
     *
     * @ORM\Column(name="Price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $price;

    /**
     * @var string|null
     *
     * @ORM\Column(name="picture", type="string", length=255, nullable=true)
     */
    private $picture;

    /**
     * @var int|null
     *
     * @ORM\Column(name="in_stock", type="integer", nullable=true)
     */
    private $inStock;
    /**
     * @var int|null
     *
     * @ORM\Column(name="in_stock_parsed", type="integer", nullable=true)
     */
    private $inStockParsed;

    /**
     * @var int
     *
     * @ORM\Column(name="supplier_id", type="integer", nullable=false)
     */
    private $supplierId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="thumbnail", type="string", length=255, nullable=true)
     */
    private $thumbnail;

    /**
     * @var int
     *
     * @ORM\Column(name="customer_votes", type="integer", nullable=false)
     */
    private $customerVotes = 100;

    /**
     * @var int
     *
     * @ORM\Column(name="items_sold", type="integer", nullable=false)
     */
    private $itemsSold = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="big_picture", type="string", length=255, nullable=true)
     */
    private $bigPicture;

    /**
     * @var int
     *
     * @ORM\Column(name="enabled", type="integer", nullable=false)
     */
    private $enabled = 1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="brief_description", type="text", length=0, nullable=true)
     */
    private $briefDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(name="list_price", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $listPrice;

    /**
     * @var string|null
     *
     * @ORM\Column(name="product_code", type="string", length=25, nullable=true, options={"fixed"=true})
     */
    private $productCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hurl", type="string", length=255, nullable=true)
     */
    private $hurl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="accompanyID", type="string", length=150, nullable=true)
     */
    private $accompanyid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="brandID", type="integer", nullable=true)
     */
    private $brandid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_title", type="string", length=255, nullable=true)
     */
    private $metaTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    private $metaKeywords;

    /**
     * @var string|null
     *
     * @ORM\Column(name="meta_desc", type="string", length=255, nullable=true)
     */
    private $metaDesc;

    /**
     * @var string|null
     *
     * @ORM\Column(name="canonical", type="string", length=255, nullable=true)
     */
    private $canonical;

    /**
     * @var string|null
     *
     * @ORM\Column(name="h1", type="string", length=255, nullable=true)
     */
    private $h1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="yml", type="integer", nullable=true, options={"default"="1"})
     */
    private $yml = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="min_qunatity", type="integer", nullable=true, options={"default"="1"})
     */
    private $minQunatity = '1';

    /**
     * @var int|null
     *
     * @ORM\Column(name="managerID", type="integer", nullable=true)
     */
    private $managerid;

    public function __construct()
    {
        $this->variants = new ArrayCollection();
    }

    public function getProductid(): ?int
    {
        return $this->productid;
    }

    public function getCategoryid(): ?int
    {
        return $this->categoryid;
    }

    public function setCategoryid(?int $categoryid): self
    {
        $this->categoryid = $categoryid;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCustomersRating(): ?float
    {
        return $this->customersRating;
    }

    public function setCustomersRating(float $customersRating): self
    {
        $this->customersRating = $customersRating;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }

    public function setPicture(?string $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getInStock(): ?int
    {
        return $this->inStock;
    }

    public function setInStock(?int $inStock): self
    {
        $this->inStock = $inStock;
        $this->setInStockParsed($inStock);
        return $this;
    }

    public function getInStockParsed(): ?int
    {
        return $this->inStockParsed;
    }

    public function setInStockParsed(?int $inStock): self
    {
        $this->inStockParsed = $inStock;
        return $this;
    }

    public function getThumbnail(): ?string
    {
        return $this->thumbnail;
    }

    public function setThumbnail(?string $thumbnail): self
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    public function getCustomerVotes(): ?int
    {
        return $this->customerVotes;
    }

    public function setCustomerVotes(int $customerVotes): self
    {
        $this->customerVotes = $customerVotes;

        return $this;
    }

    public function getItemsSold(): ?int
    {
        return $this->itemsSold;
    }

    public function setItemsSold(int $itemsSold): self
    {
        $this->itemsSold = $itemsSold;

        return $this;
    }

    public function getBigPicture(): ?string
    {
        return $this->bigPicture;
    }

    public function setBigPicture(?string $bigPicture): self
    {
        $this->bigPicture = $bigPicture;

        return $this;
    }

    public function getEnabled(): ?int
    {
        return $this->enabled;
    }

    public function setEnabled(int $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getBriefDescription(): ?string
    {
        return $this->briefDescription;
    }

    public function setBriefDescription(?string $briefDescription): self
    {
        $this->briefDescription = $briefDescription;

        return $this;
    }

    public function getListPrice(): ?string
    {
        return $this->listPrice;
    }

    public function setListPrice(?string $listPrice): self
    {
        $this->listPrice = $listPrice;

        return $this;
    }

    public function getProductCode(): ?string
    {
        return $this->productCode;
    }

    public function setProductCode(?string $productCode): self
    {
        $this->productCode = $productCode;

        return $this;
    }

    public function getHurl(): ?string
    {
        return $this->hurl;
    }

    public function setHurl(?string $hurl): self
    {
        $this->hurl = $hurl;

        return $this;
    }

    public function getAccompanyid(): ?string
    {
        return $this->accompanyid;
    }

    public function setAccompanyid(?string $accompanyid): self
    {
        $this->accompanyid = $accompanyid;

        return $this;
    }

    public function getBrandid(): ?int
    {
        return $this->brandid;
    }

    public function setBrandid(?int $brandid): self
    {
        $this->brandid = $brandid;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(?string $metaTitle): self
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(?string $metaKeywords): self
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getMetaDesc(): ?string
    {
        return $this->metaDesc;
    }

    public function setMetaDesc(?string $metaDesc): self
    {
        $this->metaDesc = $metaDesc;

        return $this;
    }

    public function getCanonical(): ?string
    {
        return $this->canonical;
    }

    public function setCanonical(?string $canonical): self
    {
        $this->canonical = $canonical;

        return $this;
    }

    public function getH1(): ?string
    {
        return $this->h1;
    }

    public function setH1(?string $h1): self
    {
        $this->h1 = $h1;

        return $this;
    }

    public function getYml(): ?int
    {
        return $this->yml;
    }

    public function setYml(?int $yml): self
    {
        $this->yml = $yml;

        return $this;
    }

    public function getMinQunatity(): ?int
    {
        return $this->minQunatity;
    }

    public function setMinQunatity(?int $minQunatity): self
    {
        $this->minQunatity = $minQunatity;

        return $this;
    }

    public function getManagerid(): ?int
    {
        return $this->managerid;
    }

    public function setManagerid(?int $managerid): self
    {
        $this->managerid = $managerid;

        return $this;
    }

    public function getCategory(): ?SsCategories
    {
        if ($this->category instanceof Proxy){
            try {
                $this->category->__load();
            } catch (EntityNotFoundException $exception){
                $this->category = null;
            }
        }
        return $this->category;
    }

    public function setCategory(?SsCategories $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getBrand(): ?SsBrand
    {
        return $this->brand;
    }

    public function setBrand(?SsBrand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @return Collection|SsProductOptionsValues[]
     */
    public function getVariants(): Collection
    {
        return $this->variants;
    }

    public function addVariant(SsProductOptionsValues $variant): self
    {
        if (!$this->variants->contains($variant)) {
            $this->variants[] = $variant;
            $variant->setProduct($this);
        }

        return $this;
    }

    public function removeVariant(SsProductOptionsValues $variant): self
    {
        if ($this->variants->contains($variant)) {
            $this->variants->removeElement($variant);
            // set the owning side to null (unless already changed)
            if ($variant->getProduct() === $this) {
                $variant->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getSupplierId(): int
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplierId
     * @return SsProducts
     */
    public function setSupplierId(int $supplierId): self
    {
        $this->supplierId = $supplierId;
        return $this;
    }


}
