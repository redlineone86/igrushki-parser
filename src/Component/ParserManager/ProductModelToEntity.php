<?php


namespace App\Component\ParserManager;


use App\Component\ParserManager\Model\ProductModelToSave;
use App\Component\UrlSlug\UrlSlug;
use App\Entity\SsProducts;
use App\Model\ProductModel;

class ProductModelToEntity
{

    const PRICE_RATE_DEFAULT = 1.2;
    /**
     * @var float
     */
    private $priceRate;
    /**
     * @var UrlSlug
     */
    private $urlSlug;

    public function __construct(UrlSlug $urlSlug, float $priceRate = self::PRICE_RATE_DEFAULT)
    {
        $this->priceRate = $priceRate;
        $this->urlSlug = $urlSlug;
    }

    /**
     * @param ProductModelToSave $productModelToSave
     * @return SsProducts
     */
    public function create(ProductModelToSave $productModelToSave): SsProducts
    {

        $product = new SsProducts();

        $model = $productModelToSave->getProductModel();
        $imgInfo = $productModelToSave->getImgInfo();

        $price = $this->getRealPrice($model);
        $url = $this->urlSlug->slug($model->getName()).'/';

        return $product
            ->setName($model->name)
            ->setPrice($price)
            ->setInStock($model->getStock())
            ->setDescription($model->getDescription())
            ->setBriefDescription($model->getDescription())
            ->setEnabled(1)
            ->setH1($model->getName())
            ->setProductCode($model->getArticle())
            ->setPicture($imgInfo->getM()->getFilename())
            ->setBigPicture($imgInfo->getB()->getFilename())
            ->setThumbnail($imgInfo->getS()->getFilename())
            ->setHurl($url)
            ->setSupplierId($model->getSupplierId())
        ;

    }

    /**
     * @param ProductModel $productModel
     * @param SsProducts $products
     */
    public function update(ProductModel $productModel, SsProducts $products): void
    {
        $price = $this->getRealPrice($productModel);
        $url = $this->urlSlug->slug($productModel->getName()).'/';

        $products
            ->setPrice($price)
            ->setInStock($productModel->getStock())
         //   ->setHurl($url)
            ->setSupplierId($productModel->getSupplierId())
        ;

    }

    /**
     * @param ProductModel $productModel
     * @return float|int
     */
    private function getRealPrice(ProductModel $productModel): float
    {
        $pr = $productModel->getPrice() * $this->priceRate;
        return ceil($pr);
    }


}
