<?php


namespace App\Component\ParserManager\Finder;


use App\Entity\SsProductOptions;
use App\Entity\SsProductsOptValVariants;
use App\Model\ProductModel;
use Doctrine\ORM\EntityManagerInterface;

class OptionFinder
{

    /**
     * @var SsProductOptions[]
     */
    private $options;

    /**
     * @var SsProductsOptValVariants[]
     */
    private $variants;

    /**
     * @var
     */
    private $variantsByProduct;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
         $this->entityManager = $entityManager;

        /**
         * @var SsProductsOptValVariants[] $variants
         */
        $variants = $entityManager->getRepository(SsProductsOptValVariants::class)->findAll();
        foreach ($variants as $variant){
            $optionName = $variant->getOptionName();
            $this->options[$optionName] = $variant->getOption();
            $this->variants[$optionName.'_'.$variant->getName()] = $variant;
        }
    }

    /**
     * @param ProductModel $productModel
     * @return SsProductsOptValVariants[]
     */
    public function findVariants(ProductModel $productModel): array
    {
        $options = $productModel->getOptions();
        foreach ($options as $optionName => $optionValue){
            if (!isset($this->options[$optionName])){
                $this->options[$optionName] = (new SsProductOptions())->setName($optionName);
                $this->entityManager->persist($this->options[$optionName]);
            }
            if (!isset($this->variants[$optionName.'_'.$optionValue])){
                $option = $this->options[$optionName];
                $this->variants[$optionName.'_'.$optionValue] = (new SsProductsOptValVariants)
                    ->setName($optionValue)
                    ->setOption($option);
                $this->entityManager->persist($this->variants[$optionName.'_'.$optionValue]);
            }

            $result[] = $this->variants[$optionName.'_'.$optionValue];
        }
        return $result ?? [];
    }
}
