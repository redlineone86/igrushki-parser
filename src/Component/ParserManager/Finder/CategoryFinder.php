<?php


namespace App\Component\ParserManager\Finder;


use App\Component\UrlSlug\UrlSlug;
use App\Entity\SsCategories;
use App\Model\ProductModel;
use Doctrine\ORM\EntityManagerInterface;

class CategoryFinder
{

    /**
     * @var SsCategories[]
     */
    private $categories;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UrlSlug
     */
    private $urlSlug;

    public function __construct(EntityManagerInterface $entityManager, UrlSlug $urlSlug)
    {
        $categories = $entityManager->getRepository(SsCategories::class)->findAll();

        foreach ($categories as $category){
            $this->categories[$category->getName()] = $category;
        }
        $this->entityManager = $entityManager;
        $this->urlSlug = $urlSlug;
    }

    /**
     * @param ProductModel $productModel
     * @return SsCategories|null
     */
    public function findOrCreate(ProductModel $productModel): ?SsCategories
    {
        $name = $productModel->getCategoryName();
        if (!is_string($name) || !trim($name)){
            return null;
        }

        $nameExplode = explode('/', $name);
        $name = trim(end($nameExplode));

        if (!isset($this->categories[$name])){
            $url = trim($this->urlSlug->slug($name), '/') . '/';
            $this->categories[$name] = (new SsCategories())->setName($name)->setHurl($url)->setParent(0)->setEnabled(0);
            $this->entityManager->persist($this->categories[$name]);
            $this->entityManager->flush();
        }

        return $this->categories[$name];
    }

}
