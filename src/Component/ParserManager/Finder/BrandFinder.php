<?php


namespace App\Component\ParserManager\Finder;


use App\Component\UrlSlug\UrlSlug;
use App\Entity\SsBrand;
use App\Model\ProductModel;
use Doctrine\ORM\EntityManagerInterface;

class BrandFinder
{

    /**
     * @var SsBrand[]
     */
    private $brands;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UrlSlug
     */
    private $urlSlug;

    public function __construct(EntityManagerInterface $entityManager, UrlSlug $urlSlug)
    {
        $brands = $entityManager->getRepository(SsBrand::class)->findAll();

        foreach ($brands as $brand){
            $this->brands[$brand->getName()] = $brand;
        }
        $this->entityManager = $entityManager;
        $this->urlSlug = $urlSlug;
    }

    /**
     * @param ProductModel $productModel
     * @return SsBrand|null
     */
    public function findOrCreate(ProductModel $productModel): ?SsBrand
    {
        $name = $productModel->getVendorName();
        if (!is_string($name) || !trim($name)){
            return null;
        }

        if (!isset($this->brands[$name])){
            $url = $this->urlSlug->slug($name);
            $this->brands[$name] = (new SsBrand())->setName($name)->setHurl($url);
            $this->entityManager->persist($this->brands[$name]);
            $this->entityManager->flush();
        }

        return $this->brands[$name];
    }

}
