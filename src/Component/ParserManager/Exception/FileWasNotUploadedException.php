<?php


namespace App\Component\ParserManager\Exception;


use Throwable;

class FileWasNotUploadedException extends \Exception
{

    /**
     * FileWasNotUploadedException constructor.
     * @param string $fileLink
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct(string $fileLink, $code = 0, Throwable $previous = null)
    {
        parent::__construct('File '.$fileLink.' was not uploaded!', $code, $previous);
    }

}
