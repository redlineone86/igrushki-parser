<?php


namespace App\Component\ParserManager\Exception;


use Throwable;

class ProductsNotLoadedException extends \Exception
{
    public function __construct(string $fileLink, $code = 0, Throwable $previous = null)
    {
        parent::__construct('Products from '.$fileLink.' file were not loaded', $code, $previous);
    }
}
