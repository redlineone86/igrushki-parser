<?php


namespace App\Component\ParserManager;


use App\Component\DownloadFile\DownloadFileInterface;
use App\Component\Gd\Gd;
use App\Component\ParserManager\Exception\FileWasNotUploadedException;
use App\Component\ParserManager\Exception\ProductsNotLoadedException;
use App\Component\ParserManager\Model\ImgInfo;
use App\Component\ParserManager\Model\ProductModelToSaveCollection;
use App\Component\ParserManager\Model\ProductModelToSave;
use App\Component\ProductFileParser\FileParserInterface;
use App\Component\Watermark\WatermarkInterface;
use App\Model\Collection\ProductCollection;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Component\Filesystem\Filesystem;

class ParserManager
{

    private const CACHE_KEY_CHAR_RESERVED = '{}()/\@:';

    private const IMG_INNER_DIR = 'img';

    /**
     * @var string
     */
    private $dir;

    /**
     * @var FileParserInterface[]
     */
    private $fileLinks = [];

    /**
     * @var [string => int]
     */
    private $fileSuppliers = [];

    /**
     * @var CacheInterface
     */
    private $cache;
    /**
     * @var DownloadFileInterface
     */
    private $downloadFile;
    /**
     * @var Filesystem
     */
    private $filesystem;
    /**
     * @var string
     */
    private $imgWatermarkDir;
    /**
     * @var WatermarkInterface
     */
    private $watermark;
    /**
     * @var Gd
     */
    private $gd;

    public function __construct(
        string $dir, string $imgWaterMarkDir,
        CacheInterface $cache,
        DownloadFileInterface $downloadFile,
        WatermarkInterface $watermark,
        Filesystem $filesystem,
        Gd $gd
    )
    {
        $this->dir = $dir;
        $this->cache = $cache;
        $this->downloadFile = $downloadFile;
        $this->filesystem = $filesystem;
        $this->imgWatermarkDir = $imgWaterMarkDir;
        $this->watermark = $watermark;
        $this->gd = $gd;
    }

    public function addFileLink(string $fileLink, FileParserInterface $fileParser, int $supplierId)
    {
        $this->fileLinks[$fileLink] = $fileParser;
        $this->fileSuppliers[$fileLink] = $supplierId;
    }

    /**
     * @param callable $observes
     * @return \SplFileInfo[]
     * @throws FileWasNotUploadedException
     */
    public function downloadFiles(callable $observes = null): array
    {
        foreach (array_keys($this->fileLinks) as $fileLink) {
            $filePath = $this->productsFilePathFromLink($fileLink);
            $file = $this->downloadFile->download($fileLink, $filePath);

            if ($observes) {
                call_user_func_array($observes, [$file]);
            }

            $result[] = $file;

        }
        return $result ?? [];
    }

    /**
     * @return ProductCollection
     * @throws FileWasNotUploadedException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function parserFiles(): ProductCollection
    {
        foreach ($this->fileLinks as $fileLink => $fileParser) {
            $filePath = $this->productsFilePathFromLink($fileLink);

            $cacheKey = $this->cacheKeyFromFileLink($fileLink);
            $this->cache->delete($cacheKey);

            /** @var ProductCollection $collection */
            $collection = $this->cache->get($cacheKey, function () use ($fileLink, $fileParser, $filePath) {
                return $fileParser->parse($filePath, $this->fileSuppliers[$fileLink]);
            });

            if (!isset($productCollection)) {
                $productCollection = $collection;
            } else {
                $productCollection->addFromCollection($collection);
            }
        }
        return $productCollection ?? new ProductCollection();
    }

    /**
     * @return ProductCollection
     * @throws ProductsNotLoadedException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getAllProductCollectionFromCache(): ProductCollection
    {
        foreach ($this->fileLinks as $fileLink => $fileParser) {

            $collection = $this->getProductCollectionFromCache($fileLink);

            if (!isset($productCollection)) {
                $productCollection = $collection;
            } else {
                $productCollection->addFromCollection($collection);
            }
        }
        return $productCollection ?? new ProductCollection();
    }

    /**
     * @param callable $observes
     * @return \SplFileInfo[]
     * @throws ProductsNotLoadedException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function downloadImages(callable $observes = null): array
    {
        $products = $this->getAllProductCollectionFromCache();
        foreach ($products as $product) {
            if (!$product->img) {
                continue;
            }

            $filePath = $this->imgFilePathFromLink($product->img);

            if (is_file($filePath)) {
                $img = new \SplFileInfo($filePath);
            } else {
                $img = $this->downloadFile->download($product->img, $this->imgFilePathFromLink($product->img));
            }

            if ($observes) {
                call_user_func_array($observes, [$img]);
            }

            $result[] = $img;
        }
        return $result ?? [];
    }

    /**
     * @param callable|null $observes
     * @return ImgInfo[]
     * @throws ProductsNotLoadedException
     * @throws \App\Component\Gd\UnknownFileExtensionException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function setWaterMark(callable $observes = null): array
    {
        $products = $this->getAllProductCollectionFromCache();
        foreach ($products as $product) {

            if (!$product->img) {
                continue;
            }

            $img = $this->imgWaterMarkFileInfoFromLink($product->img);

            try {
                if (!is_file($img->getB())) {

                    $this->watermark->set($this->imgFilePathFromLink($product->img), $img->getB());
                }
//                $this->resizeImg($img->getB(), $img->getM(), 220);
//                $this->resizeImg($img->getB(), $img->getS(), 150);
//                $this->resizeImg($img->getB(), $img->getSC(), 50);
//                $this->resizeImg($img->getB(), $img->getH(), 130);
            } catch (\Exception $exception) {
                // dd(get_class($exception));
            }

            if ($observes) {
                call_user_func_array($observes, [$img]);
            }

            $result[] = $img;
        }
        return $result ?? [];
    }

    /**
     * @return ProductModelToSaveCollection
     * @throws ProductsNotLoadedException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getProductCollectionToSave(): ProductModelToSaveCollection
    {
        $result = new ProductModelToSaveCollection();
        $products = $this->getAllProductCollectionFromCache();
        foreach ($products as $product) {

            $imgInfo = $product->img ? $this->imgWaterMarkFileInfoFromLink($product->img) : null;
            $model = new ProductModelToSave($product, $imgInfo);
            $result->add($model);
        }

        return $result;
    }


    /**
     * @param \SplFileInfo $from
     * @param \SplFileInfo $to
     * @param int $width
     * @throws \App\Component\Gd\UnknownFileExtensionException
     */
    private function resizeImg(\SplFileInfo $from, \SplFileInfo $to, int $width): void
    {
        if (!is_file($to)) {
            $this->gd->resizeImgFromPath($from->getPathname(), $to->getPathname(), $width);
        }
    }

    /**
     * @param string $fileLink
     * @param bool $raiseException
     * @return string
     * @throws FileWasNotUploadedException
     */
    private function productsFilePathFromLink(string $fileLink, bool $raiseException = false): string
    {
        $file = $this->dir() . '/' . basename($fileLink);
        if (true === $raiseException && !is_file($raiseException)) {
            throw new FileWasNotUploadedException($fileLink);
        }
        return $file;
    }

    /**
     * @param string $fileLink
     * @return string
     */
    private function imgFilePathFromLink(string $fileLink): string
    {
        return $this->dir(self::IMG_INNER_DIR) . '/' . basename($fileLink);
    }

    /**
     * @param string $fileLink
     * @return string
     */
    private function imgWaterMarkFileInfoFromLink(string $fileLink): ImgInfo
    {
        if (!is_dir($this->imgWatermarkDir)) {
            $this->filesystem->mkdir($this->imgWatermarkDir);
        }
        $filePath = $this->imgWatermarkDir . '/' . basename($fileLink);
        return new ImgInfo(new \SplFileInfo($filePath));
    }

    /**
     * @param string $fileLink
     * @return string
     */
    private function cacheKeyFromFileLink(string $fileLink): string
    {
        return str_replace(str_split(self::CACHE_KEY_CHAR_RESERVED), '_', $fileLink);
    }

    /**
     * @param string|null $innerDir
     * @return string
     */
    private function dir(?string $innerDir = null): string
    {
        $dir = null !== $innerDir ? $this->dir . '/' . $innerDir : $this->dir;
        if (!is_dir($dir)) {
            $this->filesystem->mkdir($dir);
        }
        return $dir;
    }

    /**
     * @param string $filLink
     * @return ProductCollection
     * @throws ProductsNotLoadedException
     * @throws \Psr\Cache\InvalidArgumentException
     */
    private function getProductCollectionFromCache(string $filLink): ProductCollection
    {
        $cacheKey = $this->cacheKeyFromFileLink($filLink);
        $collection = $this->cache->get($cacheKey, function () {
            return null;
        });
        if ($collection instanceof ProductCollection) {
            return $collection;
        }

        throw new ProductsNotLoadedException($filLink);

    }

}
