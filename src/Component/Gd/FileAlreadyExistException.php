<?php


namespace App\Component\Gd;


class FileAlreadyExistException extends \Exception
{

    /**
     * FileAlreadyExistException constructor.
     * @param string $filePath
     */
    public function __construct(string $filePath)
    {
        parent::__construct('File already exist '.$filePath);
    }

}
