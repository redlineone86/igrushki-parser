<?php


namespace App\Component\Gd;


use App\Component\Gd\UnknownFileExtensionException;

class Gd
{

    /**
     * @param string $filePath
     * @return false|resource|null
     * @throws UnknownFileExtensionException
     */
    public function createImageFrom(string $filePath)
    {
        $ext = $this->determineExt($filePath);
          switch ($ext){
                case '.jpg': return imagecreatefromjpeg($filePath);
                case '.png': return imagecreatefrompng($filePath);
                case '.gif': return imagecreatefromgif($filePath);
            }

        throw new UnknownFileExtensionException($filePath);
    }

    /**
     * @param $resource
     * @param $pathTo
     * @throws UnknownFileExtensionException
     */
    public function saveImage($resource, $pathTo): void
    {
        $ext = $this->determineExt($pathTo);

        switch ($ext){
            case '.jpg':
                imagejpeg($resource, $pathTo);
                break;
            case '.png':
                imagepng($resource, $pathTo);
                break;
            case '.gif':
                imagegif($resource, $pathTo);
                break;
            default:
                throw new UnknownFileExtensionException($pathTo);
        }

    }


    /**
     * @param $resource
     * @param string $fileTo
     * @param int $width
     * @param int $height
     * @throws \App\Component\Gd\UnknownFileExtensionException
     */
    public function resizeImg($resource, string $fileTo, int $width, ?int $height = null)
    {
        list($originalWidth, $originalHeight) = [imagesx($resource), imagesy($resource)];

        if (null === $height){
            $height = ($width/$originalWidth) * $originalHeight;
        }

        $thumb = imagecreatetruecolor($width, $height);

        imagecopyresized($thumb, $resource, 0, 0, 0, 0, $width, $height, $originalWidth, $originalHeight);
        $this->saveImage($thumb, $fileTo);
    }

    /**
     * @param string $filePath
     * @param string $fileTo
     * @param int $width
     * @param int|null $height
     * @throws \App\Component\Gd\UnknownFileExtensionException
     */
    public function resizeImgFromPath(string $filePath, string $fileTo, int $width, ?int $height = null)
    {
        $this->resizeImg($this->createImageFrom($filePath), $fileTo, $width, $height);
    }


    /**
     * @param string $filePath
     * @return string
     * @throws UnknownFileExtensionException
     */
    private function determineExt(string $filePath): string
    {
        preg_match('/.*(\.[^\.]+)/', $filePath, $arr);
        $ext = $arr[1] ?? null;
        if (!$ext){
            throw  new UnknownFileExtensionException($filePath);
        }
        return strtolower($ext);
    }

}
