<?php


namespace App\Component\Watermark;


class WatermarkSetDir
{

    /**
     * @var WatermarkInterface
     */
    private $watermark;

    public function __construct(WatermarkInterface $watermark)
    {
        $this->watermark = $watermark;
    }

    /**
     * @param string $dirPath
     * @param string|null $dirTo
     */
    public function set(string $dirPath, ?string $dirTo = null): void
    {
        $dirTo = $dirTo ?? $dirPath;
        foreach (scandir($dirPath) as $fileName){
            $filePath = $dirPath.'/'.$fileName;
            if (!is_file($filePath)){
                continue;
            }
            $filePathTo = $dirTo.'/'.$fileName;
            try {
                $this->watermark->set($filePath, $filePathTo);
            } catch (\Exception $exception){
                continue;
            }

        }
    }

}
