<?php


namespace App\Component\ProductFileParser;


use App\Entity\Collection\SsProductCollection;
use App\Entity\SsCategories;
use App\Model\Collection\ProductCollection;
use App\Repository\SsProductRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductModelHandler
{

    const PRICE_RATE = 1.2;

    /**
     * @var SsProductRepository
     */
    private $productRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var
     */
    private $categories;


    public function __construct(
        SsProductRepository $productRepository,
        EntityManagerInterface $entityManager
    )
    {
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    public function changeCategory(ProductCollection $productCollection, ?SsProductCollection $productEntityCollection = null)
    {
        if (null === $productEntityCollection) {
            $productEntityCollection = $this->productRepository->findByFilter(null);
        }

        $productCollection->optimizeFinder();

        foreach ($productEntityCollection as $product) {

            if (null === ($productModel = $productCollection->findElementByProperty('name', $product->getName()))) {
                continue;
            }

            if (null === $productModel->categoryName){
                continue;
            }

            $category = $this->findOrCreateCategory($productModel->categoryName);
            $product->setCategory($category);
        }

    }

    public function changePrice(ProductCollection $productCollection, ?SsProductCollection $productEntityCollection = null)
    {
        if (null === $productEntityCollection) {
            $productEntityCollection = $this->productRepository->findByFilter(null);
        }
        $productCollection->optimizeFinder();
        foreach ($productEntityCollection as $product) {
            if (null === ($productModel = $productCollection->findElementByProperty('name', $product->getName()))) {
                continue;
            }
            $price = $productModel->getPrice();
            $price *= self::PRICE_RATE;
            $product->setPrice(ceil($price));
        }
    }

    /**
     * @param string $name
     * @return SsCategories
     */
    public function findOrCreateCategory(string $name): ?SsCategories
    {

        $nameArr = explode('/', trim($name));
        $name = end($nameArr);

         $categories = $this->categories();
         if (isset($categories[$name])){
             return $categories[$name];
         }

         $category = (new SsCategories())->setName($name);
         $this->entityManager->persist($category);

         $this->categories[$category->getName()] = $category;
         return $category;
    }

    /**
     * @return SsCategories[]
     */
    private function categories(): array
    {
        if (null === $this->categories) {
            $categories = $this->entityManager->getRepository(SsCategories::class)->findAll();
            foreach ($categories as $category) {
                $this->categories[$category->getName()] = $category;
            }
        }
        return $this->categories;
    }

}
