<?php


namespace App\Component\ProductFileParser;


use App\Model\Collection\ProductCollection;
use App\Model\ProductModel;
use Symfony\Component\PropertyAccess\Exception\InvalidArgumentException;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class CsvFileParser implements FileParserInterface
{

    /**
     * @var array
     */
    private $rules;

    /**
     * @var string
     */
    private $delimiter;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;

    public function __construct(array $rules = [], string $delimiter = ';')
    {
        $this->rules = $rules;
        $this->delimiter = $delimiter;
        $this->propertyAccessor = new PropertyAccessor();
    }

    /**
     * @inheritDoc
     */
    public function parse(string $filePath, int $supplierId): ProductCollection
    {
        if (!is_file($filePath)) {
            throw new FileNotFoundException($filePath);
        }
        $data = [];
        $header = null;
        if (($handle = fopen($filePath, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $this->delimiter)) !== FALSE) {

                if (!$header) {
                    foreach ($row as $k => $r) {
                        $r = str_replace(['"', ' '], '', $r);
                        $row[$k] = trim($r);
                    }
                    $header = $row;
                    continue;
                }
             //   dd($header, $row, $filePath);
                $dataArr = array_combine($header, $row);
                $data[] = $this->createProduct($dataArr, $supplierId);
            }
            fclose($handle);

        }
        return new ProductCollection($data);
    }


    /**
     * @param array $dataArr
     * @return \App\Model\ProductModel
     */
    private function createProduct(array $dataArr, int $supplierId): ProductModel
    {
        $product = new ProductModel();

        foreach ($this->rules as $key => $attribute) {
            $value = isset($dataArr[$key]) ? $dataArr[$key] : null;

            try {

                $this->propertyAccessor->setValue($product, $attribute, $value);
            } catch (InvalidArgumentException $exception) {
                $this->propertyAccessor->setValue($product, $attribute, null);
            }
        }
        $product->setSupplierId($supplierId);
        return $product;
    }

}
