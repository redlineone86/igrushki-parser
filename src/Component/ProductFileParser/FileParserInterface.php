<?php


namespace App\Component\ProductFileParser;


use App\Model\Collection\ProductCollection;

interface FileParserInterface
{

    /**
     * @param string $filePath
     * @return ProductCollection
     */
    public function parse(string $filePath, int $supplierId): ProductCollection;

}
