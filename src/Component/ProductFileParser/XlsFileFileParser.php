<?php


namespace App\Component\ProductFileParser;


use App\Model\Collection\ProductCollection;
use App\Model\ProductModel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class XlsFileFileParser implements FileParserInterface
{

    /**
     * @var array
     */
    private $propertyMap;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;

    /**
     * @var array
     */
    private $ignoredRows;

    public function __construct(array $propertyMap = [], $ignoredRows = [1])
    {
        $this->propertyAccessor = new PropertyAccessor();
        $this->propertyMap = $propertyMap;
        $this->ignoredRows = $ignoredRows;
    }

    /**
     * @inheritDoc
     */
    public function parse(string $filePath, int $supplierId): ProductCollection
    {
        $sheet = IOFactory::createReaderForFile($filePath)->setReadDataOnly(true)->load($filePath)->getActiveSheet();

        $result = [];

        foreach ($sheet->getRowIterator() as $row) {

            if (in_array($row->getRowIndex(), $this->ignoredRows)) {
                continue;
            }
            $product = new ProductModel();

            foreach ($this->propertyMap as $id=>$property){
                $value = $sheet->getCell($id.$row->getRowIndex())->getValue();
                $this->propertyAccessor->setValue($product, $property, $value);
            }

            $product->setInStock(999);
            $product->setSupplierId($supplierId);
            $result[] = $product;
        }

        return new ProductCollection($result);
    }

}
