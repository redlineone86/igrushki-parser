<?php


namespace App\Component\ProductFileParser;




class ParserFileFactory
{


    /**
     * @param array $rules
     * @return FileParserInterface
     */
    public function makeCsvParser(array $rules): FileParserInterface
    {
        return new CsvFileParser($rules);
    }

    /**
     * @return FileParserInterface
     */
    public function makeIgrushki7Parser(): FileParserInterface
    {
        $rules = [
            "﻿Название_позиции" => "name",
            "Наименование_категории" => "categoryName",
            "Описание" => "description",
            "Вес" => "weight",
            "Длина" => "length",
            "Ширина" => "width",
            "Высота" => "height",
            "Упаковка" => "packaging",
            "Длинаупаковки" => "packingLength",
            "Ширинаупаковки" => "packingWidth",
            "Высотаупаковки" => "packingHeight",
            "Цена" => "price",
            "Ссылка_изображения" => "img",
            "Наличие" => "inStock",
            "Идентификатор_товара" => "id",
            "vendor" => "vendorName"
        ];
        return $this->makeCsvParser($rules);
    }

    /**
     * @param array $propertyMap
     * @param array $ignoredRows
     * @return FileParserInterface
     */
    public function makeXlsx(array $propertyMap, array $ignoredRows = []): FileParserInterface
    {
        return new XlsFileFileParser($propertyMap, $ignoredRows);
    }


    /**
     * @param array $ignoredRows
     * @return FileParserInterface
     */
    public function makeExportOptParser(array $ignoredRows = [1]): FileParserInterface
    {
        $propertyMap  = [
            'A' => 'id',
            'B' => 'categoryName',
            'C' => 'name',
            'D' => 'price',
            'E' => 'vendorName',
            'F' => 'article',
            'G' => 'img'
        ];
        return $this->makeXlsx($propertyMap, $ignoredRows);
    }

}
