<?php


namespace App\Model\Collection;


use App\Model\ProductModel;
use Symfony\Component\PropertyAccess\PropertyAccessor;

class ProductCollection  implements \Iterator
{

    /**
     * @var \App\Model\ProductModel[]
     */
    protected $elements = [];

    protected $elementsByProperty = [];

    /**
     * @var $this[][]
     */
    protected $collectionsByProperty = [];

    protected $properties = ['name'];

    protected $position = 0;

    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;

    public function __construct(array $array = [])
    {
        $this->elements = $array;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current(): ProductModel
    {
        return $this->elements[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->elements[$this->position]);
    }

    /**
     * @param \App\Model\ProductModel $entity
     */
    public function add(ProductModel $entity)
    {
        $this->elements[] = $entity;
    }

    /**
     * @return array|null
     */
    public function getArr(): array
    {
        return $this->elements;
    }

    /**
     * @param $entity
     * @return mixed
     */
    public function exist($entity)
    {
        $key = array_search($entity, $this->elements, true);
        if (false === $key) {
            return false;
        }
        return true;
    }

    /**
     * @param $entity
     * @return bool
     */
    public function remove($entity): bool
    {
        $key = array_search($entity, $this->elements, true);
        if (false === $key) {
            return false;
        }
        unset($this->elements[$key]);
        $this->elements = array_values($this->elements);


        return true;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->elements);
    }

    /***
     * @return int
     */
    public function ceil(): int
    {
        return ceil($this->count());
    }

    /**
     * @param ProductCollection $productCollection
     */
    public function addFromCollection(ProductCollection $productCollection): void
    {
        foreach ($productCollection as $productModel){
            $this->add($productModel);
        }
    }

    public function optimizeFinder(): void
    {
        foreach ($this->elements as $element){
            foreach ($this->properties as $property){

                $val = $this->getPropertyAccessor()->getValue($element, $property);

                $this->elementsByProperty[$property][$val] = $element;

                if (!isset($this->collectionsByProperty[$property][$val])){
                    $this->collectionsByProperty[$property][$val] = new self();
                }
                $this->collectionsByProperty[$property][$val] -> add($element);
            }
        }
    }

    /**
     * @param string $property
     * @param $value
     * @return ProductModel|null
     * @throws \Exception
     */
    public function findElementByProperty(string $property, $value): ?ProductModel
    {
        if (!isset($this->elementsByProperty[$property])){
                throw new \Exception('Search in this field is not available or optimize your search.');
        }
        return $this->elementsByProperty[$property][$value] ?? null;
    }

    /**
     * @param string $property
     * @param $value
     * @return ProductCollection|null
     * @throws \Exception
     */
    public function findCollectionByProperty(string $property, $value): ?ProductCollection
    {
        if (!isset($this->collectionsByProperty[$property])){
            throw new \Exception('Search in this field is not available or optimize your search.');
        }
        return $this->collectionsByProperty[$property][$value] ?? null;
    }


    /**
     * @return PropertyAccessor
     */
    protected function getPropertyAccessor(): PropertyAccessor
    {
        if (null === $this->propertyAccessor){
            $this->propertyAccessor = new PropertyAccessor();
        }
        return $this->propertyAccessor;
    }

}
