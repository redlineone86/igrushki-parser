<?php


namespace App\Repository\Filter;


class SsProductFilter
{

    /**
     * @var int|null
     */
    private $categoryIdMin;

    /**
     * @var int|null
     */
    private $categoryIdMax;

    /**
     * @return int|null
     */
    public function getCategoryIdMin(): ?int
    {
        return $this->categoryIdMin;
    }

    /**
     * @param int|null $categoryIdMin
     * @return SsProductFilter
     */
    public function setCategoryIdMin(?int $categoryIdMin): SsProductFilter
    {
        $this->categoryIdMin = $categoryIdMin;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCategoryIdMax(): ?int
    {
        return $this->categoryIdMax;
    }

    /**
     * @param int|null $categoryIdMax
     * @return SsProductFilter
     */
    public function setCategoryIdMax(?int $categoryIdMax): SsProductFilter
    {
        $this->categoryIdMax = $categoryIdMax;
        return $this;
    }

}
